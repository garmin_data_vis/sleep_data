// paste this into a developer tools console running in a tab at connect.garmin.com that is logged in with the same userID as the userID specified below
userID = null;
fetch('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js')
    .then(response => response.text())
    .then(text => eval(text))
    .then(() => {
        var getDaysBetweenDates = function(startDate, endDate) {
            var now = startDate.clone(), dates = [];
            while (now.isSameOrBefore(endDate)) {
                dates.push(now.format('YYYY-MM-DD'));
                now.add(1, 'days');
            }
            return dates;
        };

        var startDate = moment('2017-07-31');
        var endDate = moment('2023-02-20');
        var dateList = getDaysBetweenDates(startDate, endDate);

        var sleepData = {};
        for (let d = 0; d < dateList.length; d ++) {
            jQuery.getJSON(
                'https://connect.garmin.com/modern/proxy/wellness-service/wellness/dailySleepData/' + userID + '?date=' + dateList[d],
                (i) => {
                    sleepData[dateList[d]] = {
                        sleepStartLocal: i.dailySleepDTO.sleepStartTimestampLocal,
                        sleepEndLocal: i.dailySleepDTO.sleepEndTimestampLocal,
                        sleepStartGMT: i.dailySleepDTO.sleepStartTimestampGMT,
                        sleepEndGMT: i.dailySleepDTO.sleepEndTimestampGMT,
                        awakeSleepSeconds: i.dailySleepDTO.awakeSleepSeconds,
                        awakeCount: i.dailySleepDTO.awakeCount,
                        deepSleepSeconds: i.dailySleepDTO.deepSleepSeconds,
                        remSleepSeconds: i.dailySleepDTO.remSleepSeconds,
                        lightSleepSeconds: i.dailySleepDTO.lightSleepSeconds,
                        sleepTimeSeconds: i.dailySleepDTO.sleepTimeSeconds
                    };
                }
            );
        }
        console.log(sleepData);
    })
