import json
import datetime as dt
import pytz as tz
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import math
import pandas as pd
import seaborn as sns

colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

dates = []
total_time = [] # hr
deep_time = [] # min
light_time = [] # min
rem_time = [] # min
awake_time = [] # min
sleep_time = [] # timestamp
wake_time = [] # timestamp

localtime = tz.timezone('US/Eastern')

with open('sleep_data_compact_2017_07_31_to_2023_02_20.json') as f:
    data = json.load(f)
    for k,v in data.items():
        if v['sleepTimeSeconds'] is None:
            continue
        if v['remSleepSeconds'] is None or v['remSleepSeconds'] == 0:
            continue
        date = dt.datetime.strptime(k, '%Y-%m-%d')
        if date < dt.datetime(2018, 7, 1):
            continue
        dates.append(date)
        total_time.append(v['sleepTimeSeconds']/3600)
        deep_time.append(v['deepSleepSeconds']/3600)
        light_time.append(v['lightSleepSeconds']/3600)
        rem_time.append(v['remSleepSeconds']/3600)
        awake_time.append(v['awakeSleepSeconds']/3600)
        t_local = dt.datetime.fromtimestamp(v['sleepStartLocal']/1000)
        t_gmt = dt.datetime.fromtimestamp(v['sleepStartGMT']/1000)
        d_hours = ((t_gmt - t_local + localtime.localize(t_gmt).dst()).seconds/3600 - 5)
        # garmin messed up the timestamps, so do some hacky workaround to get the right time
        ts_sleep = dt.datetime.fromtimestamp(v['sleepStartGMT']/1000 - d_hours*3600)
        ts_wake = dt.datetime.fromtimestamp(v['sleepEndGMT']/1000 - d_hours*3600)
        d_wake = 2
        d_sleep = 2 if ts_sleep.day == ts_wake.day else 1
        sleep_time.append(ts_sleep.replace(year=1970, month=1, day=d_sleep))
        wake_time.append(ts_wake.replace(year=1970, month=1, day=d_wake))

# get all timeseries data
total_time = pd.Series([s for _, s in sorted(zip(dates, total_time))])
deep_time = pd.Series([s for _, s in sorted(zip(dates, deep_time))])
light_time = pd.Series([s for _, s in sorted(zip(dates, light_time))])
rem_time = pd.Series([s for _, s in sorted(zip(dates, rem_time))])
awake_time = pd.Series([s for _, s in sorted(zip(dates, awake_time))])
sleep_time = pd.Series([s for _, s in sorted(zip(dates, sleep_time))])
wake_time = pd.Series([s for _, s in sorted(zip(dates, wake_time))])
# again, hacky since the timestamps aren't right
sleep_time_unix = pd.Series([t.timestamp() + 5*3600 for t in sleep_time])
wake_time_unix = pd.Series([t.timestamp() + 5*3600 for t in wake_time])
# total sleep (hrs), deep (min), light (min), rem (min), awake (min)
data = [total_time, deep_time, light_time, rem_time, awake_time]
ylabels = ['total sleep [hr]', 'deep sleep[hr]', 'light sleep [hr]', 'rem sleep [hr]', 'awake [hr]']
dates = pd.Series(sorted(dates))

window_size = 14


data_mean = [d.rolling(window=window_size).mean() for d in data]
data_std = [d.rolling(window=window_size).std() for d in data]
data_minimum = [d.rolling(window=window_size).min() for d in data]
data_maximum = [d.rolling(window=window_size).max() for d in data]

sr = sleep_time_unix.rolling(window=window_size)
wr = wake_time_unix.rolling(window=window_size)
mean_sleep_unix = sr.mean()
mean_wake_unix = wr.mean()
lower_sleep_unix = np.maximum(mean_sleep_unix - sr.std(), sr.min())
upper_sleep_unix = np.minimum(mean_sleep_unix + sr.std(), sr.max())
lower_wake_unix = np.maximum(mean_wake_unix - wr.std(), wr.min())
upper_wake_unix = np.minimum(mean_wake_unix + wr.std(), wr.max())
dates_trunc = [d for d,m in zip(dates, mean_sleep_unix) if not(math.isnan(m))]
mean_sleep = [dt.datetime.fromtimestamp(int(round(ts))) for ts in mean_sleep_unix if not(math.isnan(ts))]
mean_wake = [dt.datetime.fromtimestamp(int(round(ts))) for ts in mean_wake_unix if not(math.isnan(ts))]
lower_sleep = [dt.datetime.fromtimestamp(int(round(ts))) for ts in lower_sleep_unix if not(math.isnan(ts))]
upper_sleep = [dt.datetime.fromtimestamp(int(round(ts))) for ts in upper_sleep_unix if not(math.isnan(ts))]
lower_wake = [dt.datetime.fromtimestamp(int(round(ts))) for ts in lower_wake_unix if not(math.isnan(ts))]
upper_wake = [dt.datetime.fromtimestamp(int(round(ts))) for ts in upper_wake_unix if not(math.isnan(ts))]

# aggregate data on a per year and per weekday basis
per_year_sleep_time = [[], [], [], []]
per_year_wake_time = [[], [], [], []]
per_year_data = [[], [], [], []]
per_weekday_data = [[], [], [], [], [], [], []]
for d, date in enumerate(dates):
    w = date.weekday()
    if (date.year >= 2019) and (date.year <= 2022):
        if len(per_year_data[date.year-2019]) == 0:
            per_year_data[date.year-2019] = [[], [], [], [], []]
        for i in range(5):
            per_year_data[date.year-2019][i].append(data[i][d])
        per_year_sleep_time[date.year-2019].append(sleep_time[d])
        per_year_wake_time[date.year-2019].append(wake_time[d])
    if len(per_weekday_data[w]) == 0:
        per_weekday_data[w] = [[], [], [], [], []]
    for i in range(5):
        if len(per_weekday_data[w][i]) == 0:
            per_weekday_data[w][i] = [[], [], [], []]
        if (date.year >= 2019) and (date.year <= 2022):
            per_weekday_data[w][i][date.year-2019].append(data[i][d])

fig, ax = plt.subplots(6, 2, sharey='row', gridspec_kw={'width_ratios': [6,1], 'height_ratios': [2,1,1,1,1,1]})
#ax[0,0].get_shared_x_axes().join(ax[0,0], *ax[1:,0])
#ax[4,0].xaxis.set_major_formatter(mdates.DateFormatter('%Y/%m/%d'))
notable_spans = [
    (dt.datetime(2019,3,3), dt.datetime(2019,3,20)),
    (dt.datetime(2019,5,26), dt.datetime(2019,8,16)),
    (dt.datetime(2021,1,12), dt.datetime(2021,2,5)),
    (dt.datetime(2021,6,10), dt.datetime(2021,6,22)),
    (dt.datetime(2021,12,8), dt.datetime(2021,12,11)),
    (dt.datetime(2022,5,3), dt.datetime(2022,5,10)),
    (dt.datetime(2022,6,3), dt.datetime(2022,6,23)),
    #(dt.datetime(2022,12,11), dt.datetime(2022,12,15))
]
default_anno_height = dt.datetime(1970,1,1,19)
span_annotations = [
    ('first breakup', (dt.datetime(2019,3,3), default_anno_height), (-75, 0)),
    ('summer job\nafter freshman year', (dt.datetime(2019,8,16), default_anno_height), (10, 0)),
    ('got covid', (dt.datetime(2021,1,12), default_anno_height), (-55, 0)),
    ('home after\na tough\nsemester', (dt.datetime(2021,6,22), default_anno_height), (10, 0)),
    ('final\nprojects', (dt.datetime(2021,12,11), default_anno_height), (10, 0)),
    ('', (dt.datetime(2022,5,3), default_anno_height), (-10, 0)),
    #('senior trip', (dt.datetime(2022,6,23), 3.2), (10, 0)),
    ('senior trip', (dt.datetime(2022,6,23), default_anno_height), (10, 0)),
    #('final project', (dt.datetime(2022,12,11), 2.0), (-70, 0)),
]
#notable_dates = [dt.datetime(2019,3,3), dt.datetime(2021,1,12), dt.datetime(2021,12,9)]
for i in range(6):
    if i == 0:
        ax[i,0].yaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
        ax[i,0].plot(dates, sleep_time, '.', markersize=3, label='sleep start', color=colors[0])
        ax[i,0].plot(dates, wake_time, '.', markersize=3, label='sleep end', color=colors[1])
        ax[i,0].plot(dates_trunc, mean_sleep, color='k')
        ax[i,0].plot(dates_trunc, mean_wake, color='k')
        ax[i,0].fill_between(dates_trunc, lower_sleep, upper_sleep, color=colors[0], alpha=0.3)
        ax[i,0].fill_between(dates_trunc, lower_wake, upper_wake, color=colors[1], alpha=0.3)
        ax[i,0].set_ylabel('sleep start/end time')
        for j in range(4):
            sns.kdeplot(data=pd.DataFrame(per_year_sleep_time[j], columns=['a']), color=colors[j], y='a', ax=ax[i,1], fill=True)
            sns.kdeplot(data=pd.DataFrame(per_year_wake_time[j], columns=['a']), color=colors[j], y='a', ax=ax[i,1], fill=True, label='_nolegend_')
        ax[i,1].yaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    else:
        ax[i,0].sharex(ax[0,0])
        ax[i,0].plot(dates, data[i-1], '.', label=ylabels[i-1][:-4], markersize=3)
        ax[i,0].fill_between(dates, np.maximum(data_mean[i-1]-data_std[i-1], data_minimum[i-1]), np.minimum(data_mean[i-1]+data_std[i-1], data_maximum[i-1]), alpha=0.3)
        ax[i,0].plot(dates, data[i-1].rolling(window=window_size).mean(), color='k')
        ax[i,0].set_ylabel(ylabels[i-1])
        for j in range(4):
            p = sns.kdeplot(data=pd.DataFrame(per_year_data[j][i-1], columns=['a']), y="a", ax=ax[i,1], fill=True)
    for dspan in notable_spans:
        ax[i,0].axvspan(dspan[0], dspan[1], alpha=0.1, color="black")


    ax[i,1].tick_params(bottom=False, labelbottom=False)
    ax[i,1].set_xlabel(None)
    ax[i,1].yaxis.tick_right()
    ax[i,1].yaxis.set_tick_params(labelright=True)

ax[0,0].set_title(f'Four-year sleep patterns of college undergrad ({window_size}-day moving average shown in black)')
ax[0,1].set_title('Per-year distributions')
# annotate interesting time periods
anno_arrow_props = {'facecolor':'black', 'shrink':0.05, 'width':0.5, 'headwidth':2, 'headlength':4}
for anno in span_annotations:
    ax[0,0].annotate(anno[0], xy=anno[1], xytext=anno[2], textcoords='offset points', arrowprops=anno_arrow_props, va='center')
# annotate zoom classes
ax[0,0].annotate('west-coast zoom class', xy=(dt.datetime(2020,4,15), dt.datetime(1970,1,2,7,30)), xytext=(dt.datetime(2020,8,7), dt.datetime(1970,1,2,5,30)), textcoords='data', arrowprops=anno_arrow_props, va='center', ha='center')
ax[0,0].annotate('', xy=(dt.datetime(2020,10,25), dt.datetime(1970,1,2,7)), xytext=(dt.datetime(2020,12,7), dt.datetime(1970,1,2,6,15)), textcoords='data', arrowprops=anno_arrow_props, va='center', ha='center')

for i in range(6):
    ax[i,0].legend(loc='upper left', ncols=2)
ax[0,1].legend([2019, 2020, 2021, 2022], loc='upper right')

fig.align_ylabels(ax)
plt.subplots_adjust(wspace=0, hspace=0)
plt.show()
